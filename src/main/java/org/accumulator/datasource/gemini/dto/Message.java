package org.accumulator.datasource.gemini.dto;


import java.util.List;

public class Message {
    private String type;
    private Long eventId;
    private Long timestamp;
    private Long timestampms;
    private Long socket_sequence;
    private List<Event> events;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getTimestampms() {
        return timestampms;
    }

    public void setTimestampms(Long timestampms) {
        this.timestampms = timestampms;
    }

    public Long getSocket_sequence() {
        return socket_sequence;
    }

    public void setSocket_sequence(Long socket_sequence) {
        this.socket_sequence = socket_sequence;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
