package org.accumulator.datasource.gemini.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import org.accumulator.datasource.TradeMessage;
import org.accumulator.datasource.gemini.dto.Message;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.LoggerFactory;


public class KafkaSender {
    private ObjectMapper objectMapper = new ObjectMapper();

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(KafkaSender.class);

    private Producer<String, TradeMessage> producer;
    private WebSocket ws;

    public KafkaSender(Producer<String, TradeMessage> producer, WebSocket ws) {
        this.producer = producer;
        this.ws = ws;
    }

    public void setListener(String topic){
        ws.addListener(new WebSocketAdapter() {
            @Override
            public void onTextMessage(WebSocket websocket, String message) throws Exception {
                Message msg = objectMapper.readValue(message, Message.class);
                msg
                        .getEvents()
                        .stream()
                        .filter(event -> event.getType().equals("trade"))
                        .map(event -> TradeMessage.newBuilder()
                                .setPrice(event.getPrice())
                                .setQuantity(event.getAmount())
                                .setSide(event.getSide())
                                .setTimeStamp(msg.getTimestampms())
                                .build()
                        )
                        .forEach(event -> {
                            producer.send(new ProducerRecord<>(topic, msg.getEventId().toString(), event));
                            logger.info(event.getQuantity()+ " bitcoins was sold at "+event.getPrice() + "$");
                            }
                        );
            }
        });
    }
}
