package org.accumulator.datasource.gemini;

import com.neovisionaries.ws.client.*;
import org.accumulator.datasource.TradeMessage;
import org.accumulator.datasource.gemini.service.KafkaProducerSimpleFactory;
import org.accumulator.datasource.gemini.service.KafkaSender;
import org.apache.kafka.clients.producer.Producer;
import org.apache.log4j.BasicConfigurator;

import java.io.IOException;

public class App {
    private static final String server = "wss://api.gemini.com/v1/marketdata/BTCUSD";
    private static final int timeout = 5000;

    public static void main(String[] args) {
        String kafkaHost = System.getenv("KAFKA_HOST");
        Integer kafkaPort = Integer.valueOf(System.getenv("KAFKA_PORT"));
        String topic = System.getenv("KAFKA_TOPIC");
        String bootstrapServer = kafkaHost + ":" + kafkaPort;


        BasicConfigurator.configure();
        try {
            Producer<String, TradeMessage> producer = new KafkaProducerSimpleFactory(bootstrapServer).getProducer();
            WebSocket webSocket = new WebSocketFactory()
                    .setConnectionTimeout(timeout)
                    .createSocket(server)
                    .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                    .connect();

            KafkaSender kafkaSender = new KafkaSender(producer, webSocket);
            kafkaSender.setListener(topic);
        }  catch (WebSocketException | IOException e) {
            e.printStackTrace();
        }
    }
}
