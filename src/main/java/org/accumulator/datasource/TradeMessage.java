/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package org.accumulator.datasource;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class TradeMessage extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 7594289421542852611L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"TradeMessage\",\"namespace\":\"org.accumulator.datasource\",\"fields\":[{\"name\":\"price\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"quantity\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"side\",\"type\":[\"null\",{\"type\":\"string\",\"avro.java.string\":\"String\"}]},{\"name\":\"timeStamp\",\"type\":\"long\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<TradeMessage> ENCODER =
      new BinaryMessageEncoder<TradeMessage>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<TradeMessage> DECODER =
      new BinaryMessageDecoder<TradeMessage>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<TradeMessage> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<TradeMessage> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<TradeMessage>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this TradeMessage to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a TradeMessage from a ByteBuffer. */
  public static TradeMessage fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.String price;
  @Deprecated public java.lang.String quantity;
  @Deprecated public java.lang.String side;
  @Deprecated public long timeStamp;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public TradeMessage() {}

  /**
   * All-args constructor.
   * @param price The new value for price
   * @param quantity The new value for quantity
   * @param side The new value for side
   * @param timeStamp The new value for timeStamp
   */
  public TradeMessage(java.lang.String price, java.lang.String quantity, java.lang.String side, java.lang.Long timeStamp) {
    this.price = price;
    this.quantity = quantity;
    this.side = side;
    this.timeStamp = timeStamp;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return price;
    case 1: return quantity;
    case 2: return side;
    case 3: return timeStamp;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: price = (java.lang.String)value$; break;
    case 1: quantity = (java.lang.String)value$; break;
    case 2: side = (java.lang.String)value$; break;
    case 3: timeStamp = (java.lang.Long)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'price' field.
   * @return The value of the 'price' field.
   */
  public java.lang.String getPrice() {
    return price;
  }

  /**
   * Sets the value of the 'price' field.
   * @param value the value to set.
   */
  public void setPrice(java.lang.String value) {
    this.price = value;
  }

  /**
   * Gets the value of the 'quantity' field.
   * @return The value of the 'quantity' field.
   */
  public java.lang.String getQuantity() {
    return quantity;
  }

  /**
   * Sets the value of the 'quantity' field.
   * @param value the value to set.
   */
  public void setQuantity(java.lang.String value) {
    this.quantity = value;
  }

  /**
   * Gets the value of the 'side' field.
   * @return The value of the 'side' field.
   */
  public java.lang.String getSide() {
    return side;
  }

  /**
   * Sets the value of the 'side' field.
   * @param value the value to set.
   */
  public void setSide(java.lang.String value) {
    this.side = value;
  }

  /**
   * Gets the value of the 'timeStamp' field.
   * @return The value of the 'timeStamp' field.
   */
  public java.lang.Long getTimeStamp() {
    return timeStamp;
  }

  /**
   * Sets the value of the 'timeStamp' field.
   * @param value the value to set.
   */
  public void setTimeStamp(java.lang.Long value) {
    this.timeStamp = value;
  }

  /**
   * Creates a new TradeMessage RecordBuilder.
   * @return A new TradeMessage RecordBuilder
   */
  public static org.accumulator.datasource.TradeMessage.Builder newBuilder() {
    return new org.accumulator.datasource.TradeMessage.Builder();
  }

  /**
   * Creates a new TradeMessage RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new TradeMessage RecordBuilder
   */
  public static org.accumulator.datasource.TradeMessage.Builder newBuilder(org.accumulator.datasource.TradeMessage.Builder other) {
    return new org.accumulator.datasource.TradeMessage.Builder(other);
  }

  /**
   * Creates a new TradeMessage RecordBuilder by copying an existing TradeMessage instance.
   * @param other The existing instance to copy.
   * @return A new TradeMessage RecordBuilder
   */
  public static org.accumulator.datasource.TradeMessage.Builder newBuilder(org.accumulator.datasource.TradeMessage other) {
    return new org.accumulator.datasource.TradeMessage.Builder(other);
  }

  /**
   * RecordBuilder for TradeMessage instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<TradeMessage>
    implements org.apache.avro.data.RecordBuilder<TradeMessage> {

    private java.lang.String price;
    private java.lang.String quantity;
    private java.lang.String side;
    private long timeStamp;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(org.accumulator.datasource.TradeMessage.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.price)) {
        this.price = data().deepCopy(fields()[0].schema(), other.price);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.quantity)) {
        this.quantity = data().deepCopy(fields()[1].schema(), other.quantity);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.side)) {
        this.side = data().deepCopy(fields()[2].schema(), other.side);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.timeStamp)) {
        this.timeStamp = data().deepCopy(fields()[3].schema(), other.timeStamp);
        fieldSetFlags()[3] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing TradeMessage instance
     * @param other The existing instance to copy.
     */
    private Builder(org.accumulator.datasource.TradeMessage other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.price)) {
        this.price = data().deepCopy(fields()[0].schema(), other.price);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.quantity)) {
        this.quantity = data().deepCopy(fields()[1].schema(), other.quantity);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.side)) {
        this.side = data().deepCopy(fields()[2].schema(), other.side);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.timeStamp)) {
        this.timeStamp = data().deepCopy(fields()[3].schema(), other.timeStamp);
        fieldSetFlags()[3] = true;
      }
    }

    /**
      * Gets the value of the 'price' field.
      * @return The value.
      */
    public java.lang.String getPrice() {
      return price;
    }

    /**
      * Sets the value of the 'price' field.
      * @param value The value of 'price'.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder setPrice(java.lang.String value) {
      validate(fields()[0], value);
      this.price = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'price' field has been set.
      * @return True if the 'price' field has been set, false otherwise.
      */
    public boolean hasPrice() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'price' field.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder clearPrice() {
      price = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'quantity' field.
      * @return The value.
      */
    public java.lang.String getQuantity() {
      return quantity;
    }

    /**
      * Sets the value of the 'quantity' field.
      * @param value The value of 'quantity'.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder setQuantity(java.lang.String value) {
      validate(fields()[1], value);
      this.quantity = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'quantity' field has been set.
      * @return True if the 'quantity' field has been set, false otherwise.
      */
    public boolean hasQuantity() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'quantity' field.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder clearQuantity() {
      quantity = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'side' field.
      * @return The value.
      */
    public java.lang.String getSide() {
      return side;
    }

    /**
      * Sets the value of the 'side' field.
      * @param value The value of 'side'.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder setSide(java.lang.String value) {
      validate(fields()[2], value);
      this.side = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'side' field has been set.
      * @return True if the 'side' field has been set, false otherwise.
      */
    public boolean hasSide() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'side' field.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder clearSide() {
      side = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'timeStamp' field.
      * @return The value.
      */
    public java.lang.Long getTimeStamp() {
      return timeStamp;
    }

    /**
      * Sets the value of the 'timeStamp' field.
      * @param value The value of 'timeStamp'.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder setTimeStamp(long value) {
      validate(fields()[3], value);
      this.timeStamp = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'timeStamp' field has been set.
      * @return True if the 'timeStamp' field has been set, false otherwise.
      */
    public boolean hasTimeStamp() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'timeStamp' field.
      * @return This builder.
      */
    public org.accumulator.datasource.TradeMessage.Builder clearTimeStamp() {
      fieldSetFlags()[3] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public TradeMessage build() {
      try {
        TradeMessage record = new TradeMessage();
        record.price = fieldSetFlags()[0] ? this.price : (java.lang.String) defaultValue(fields()[0]);
        record.quantity = fieldSetFlags()[1] ? this.quantity : (java.lang.String) defaultValue(fields()[1]);
        record.side = fieldSetFlags()[2] ? this.side : (java.lang.String) defaultValue(fields()[2]);
        record.timeStamp = fieldSetFlags()[3] ? this.timeStamp : (java.lang.Long) defaultValue(fields()[3]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<TradeMessage>
    WRITER$ = (org.apache.avro.io.DatumWriter<TradeMessage>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<TradeMessage>
    READER$ = (org.apache.avro.io.DatumReader<TradeMessage>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
